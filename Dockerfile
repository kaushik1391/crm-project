#docker file
FROM node:16
#working dir
WORKDIR /home/ubuntu/CRM-Software
COPY . .
RUN npm install
CMD ["node" , "app.js"]
